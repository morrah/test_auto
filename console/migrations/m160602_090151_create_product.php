<?php

use yii\db\Migration;

/**
 * Handles the creation for table `{{%product}}`.
 */
class m160602_090151_create_product extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('{{%product}}', [
            'id' => $this->primaryKey(),
            'title' => $this->string(250),
            'in_stock' => $this->integer(11),
            'price' => $this->decimal(15,2),
            'created_at' => $this->integer(11),
            'updated_at' => $this->integer(11),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('{{%product}}');
    }
}
