<?php

use yii\db\Migration;

/**
 * Handles the creation for table `{{%view_report}}`.
 */
class m160602_092237_create_view_report extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
      $this->db->createCommand('CREATE VIEW report  AS
        SELECT u.id, u.username, u.email, u.status, o.id AS order_id, o.total_quantity, o.amount, o.note, i.id_product, i.quantity, i.price, p.title
        FROM ' . $this->db->quoteTableName('{{%order}}') . ' AS o
        LEFT JOIN ' . $this->db->quoteTableName('{{%user}}') . ' AS u
        ON u.id = o.id_user
        LEFT JOIN ' . $this->db->quoteTableName('{{%order_product}}') . ' AS i
        ON o.id = i.id_order
        LEFT JOIN ' . $this->db->quoteTableName('{{%product}}') . ' AS p
        ON i.id_product = p.id
      ')->execute();
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->db->createCommand('DROP VIEW report')->execute();
    }
}
