<?php

use yii\db\Migration;

/**
 * Handles the creation for table `{{%order}}`.
 */
class m160602_091105_create_order extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('{{%order}}', [
            'id' => $this->primaryKey(),
            'id_user' => $this->integer(11),
            'total_quantity' => $this->integer(11),
            'amount' => $this->decimal(15,2),
            'note' => $this->string(250),
            'created_at' => $this->integer(11),
            'updated_at' => $this->integer(11),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('{{%order}}');
    }
}
