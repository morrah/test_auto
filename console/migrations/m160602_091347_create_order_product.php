<?php

use yii\db\Migration;
use common\models\User;

/**
 * Handles the creation for table `{{%order_product}}`.
 */
class m160602_091347_create_order_product extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('{{%order_product}}', [
            'id' => $this->primaryKey(),
            'id_order' => $this->integer(11),
            'id_product' => $this->integer(11),
            'quantity' => $this->integer(11),
            'price' => $this->decimal(15,2),
            'created_at' => $this->integer(11),
            'updated_at' => $this->integer(11),
        ]);

        $this->seed();
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('{{%order_product}}');
    }

    // Let's play a fair play and do not use the fixtures due to they are based on ActivaRecord :)
    private function seed(){
      $fixt = $this->makeFixt('product');
      $sql = $this->db->getQueryBuilder()->batchInsert($this->db->quoteTableName('{{%product}}'), ['title','in_stock','price','created_at'], $fixt);//->execute();
      $res = $this->db->createCommand($sql)->execute();

      $prod_ids = $this->db->createCommand('SELECT id, in_stock, price from '.$this->db->quoteTableName('{{%product}}'))->query();
      if(!$prod_ids || !isset($prod_ids->rowCount))
        return;

      $prod_ids = $prod_ids->readAll();//var_dump($prod_ids);

      $this->makeFixt('user', ['products' => $prod_ids]);

      $user_ids = $this->db->createCommand('SELECT id from '.$this->db->quoteTableName('{{%user}}'))->query();

      if(!$user_ids || !isset($user_ids->rowCount))
        return;

      $user_ids = $user_ids->readAll();

      $fixt = $this->makeFixt('order', ['products' => $prod_ids, 'users' => $user_ids]);

      $sql = $this->db->getQueryBuilder()->batchInsert($this->db->quoteTableName('{{%order}}'), ['id_user','total_quantity','amount','note','created_at'], $fixt);//->execute();
      $res = $this->db->createCommand($sql)->execute();

      $order_ids = $this->db->createCommand('SELECT id from '.$this->db->quoteTableName('{{%order}}'))->query();

      if(!$order_ids || !isset($order_ids->rowCount))
        return;

      $order_ids = $order_ids->readAll();

      $fixt = $this->makeFixt('order_product', ['products' => $prod_ids, 'users' => $user_ids, 'orders' => $order_ids]);

      $sql = $this->db->getQueryBuilder()->batchInsert($this->db->quoteTableName('{{%order_product}}'), ['id_order','id_product','quantity','price','created_at'], $fixt);//->execute();
      $res = $this->db->createCommand($sql)->execute();

      $orders_data = $this->db->createCommand('SELECT id_order, SUM(quantity) AS total_quantity, SUM(quantity*price) AS amount from '.$this->db->quoteTableName('{{%order_product}} GROUP BY id_order'))->query();

      if(!$orders_data || !isset($orders_data->rowCount))
        return;
/// Знаю, что криво, надоело ковыряться с "честной" фикстурой, не использующей ActiveRecord. Хочу скорее попробовать вьюшки в PostgreSQL!!!
      echo "Update orders table...\r\n";
      foreach($orders_data as $order){
        $id = $order['id_order'];
        unset($order['id_order']);
        var_dump($order);
        $params = [];
        $sql = $this->db->getQueryBuilder()->update($this->db->quoteTableName('{{%order}}'), $order, "id = $id", $params);//->execute();
        echo $sql."\r\n";
        $res = $this->db->createCommand($sql,$params)->execute();
      }

    }

// Don't make existence of necessary data to save time
    private function makeFixt($call = 'product', $ids = []){
      $vals = [];
      switch($call){
        case 'product':
          for($i=1;$i<=100;$i++){
            $vals[] = [
              'title' => "Prod_$i",
              'in_stock' => rand(1,100),
              'price' => rand(10,10000)/100,
              'created_at' => intval(time()),
            ];
          }
          break;
        case 'order':
          for($i=1;$i<=100;$i++){
            $vals[] = [
              'id_user' => $ids['users'][rand(1,count($ids['users'])-1)]['id'],
              'total_quantity' => 0,
              'amount' => 0,
              'note' => "Test order #$i",
              'created_at' => intval(time()),
            ];
          }
          break;
        case 'order_product':
            for($i=1;$i<=100;$i++){
              $vals[] = [
                'id_order' => $ids['orders'][rand(1,count($ids['orders'])-1)]['id'],
                'id_product' => $ids['products'][rand(1,count($ids['products'])-1)]['id'],
                'quantity' => rand(1,5),
                'price' => $ids['products'][rand(1,count($ids['products'])-1)]['price'],
                'created_at' => intval(time()),
              ];
            }
          break;
        case 'user':
          for($i=1;$i<=20;$i++){
            $user = new User();
            $user->username = "User_$i";
            $user->email = "User_$i@example.com";
            $user->setPassword("User_$i");
            $user->generateAuthKey();
            $vals[] =  $user->save() ? $user : null;
          }
          break;
        default:
          break;
      }
      return $vals;
    }

}
