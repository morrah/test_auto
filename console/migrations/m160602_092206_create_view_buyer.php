<?php

use yii\db\Migration;

/**
 * Handles the creation for table `{{%view_buyer}}`.
 */
class m160602_092206_create_view_buyer extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->db->createCommand('CREATE VIEW buyer  AS
          SELECT u.id, u.username, u.email, u.status, o.id AS order_id, o.total_quantity, o.amount, o.note
          FROM ' . $this->db->quoteTableName('{{%order}}') . ' AS o
          LEFT JOIN ' . $this->db->quoteTableName('{{%user}}') . ' AS u
          ON u.id = o.id_user
        ')->execute();
        $this->db->createCommand('CREATE VIEW buyer_grouped  AS
          SELECT u.id, u.username, u.email, u.status, COUNT(o.id), SUM(o.total_quantity) AS total_quantity, SUM(o.amount) AS amount
          FROM ' . $this->db->quoteTableName('{{%order}}') . ' AS o
          LEFT JOIN ' . $this->db->quoteTableName('{{%user}}') . ' AS u
          ON u.id = o.id_user
          GROUP BY u.id
        ')->execute();
        /*$this->createTable('{{%view_buyer}}', [
            'id' => $this->primaryKey(),
        ]);*/
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
      $this->db->createCommand('DROP VIEW buyer')->execute();
      $this->db->createCommand('DROP VIEW buyer_grouped')->execute();
    }
}
