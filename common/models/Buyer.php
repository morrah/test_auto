<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%buyer}}".
 *
 * @property integer $id
 * @property string $username
 * @property string $email
 * @property integer $status
 * @property integer $order_id
 * @property integer $total_quantity
 * @property string $amount
 * @property string $note
 */
class Buyer extends Model
{

    public $id;
    public $username;
    public $email;
    public $status;
    public $order_id;
    public $total_quantity;
    public $amount;
    public $note;
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return Yii::$app->db->quoteTableName('{{%buyer}}');
    }

    /**
     * @inheritdoc
     */
    public function attributes()
    {
        return [
            'id',
            'username',
            'email',
            'status',
            'order_id',
            'total_quantity',
            'amount',
            'note',
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'username' => Yii::t('app', 'Username'),
            'email' => Yii::t('app', 'Email'),
            'status' => Yii::t('app', 'Status'),
            'order_id' => Yii::t('app', 'Order ID'),
            'total_quantity' => Yii::t('app', 'Total Quantity'),
            'amount' => Yii::t('app', 'Amount'),
            'note' => Yii::t('app', 'Note'),
        ];
    }
}
