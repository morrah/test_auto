<?php

namespace common\models;

use Yii;

/**
 * This is the model class for PostgreSQL views.
 *
 * @property integer $id
 * @property string $title
 * @property integer $owner
 */
class View extends Model
{

    public $id;

    public $title;

    public $owner;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'view';
    }

    /**
     * @inheritdoc
     */
    public function attributes()
    {
        return [
            'id',
            'title',
            'owner',
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'title' => Yii::t('app', 'Title'),
            'owner' => Yii::t('app', 'Owner'),
        ];
    }

    public function getAll(){
      $pg_views = $this->db->createCommand("select c.relname AS \"title\", c.relkind AS \"type\", r.rolname AS \"owner\" from pg_catalog.pg_class AS c join pg_catalog.pg_roles AS r on r.oid=c.relowner left join pg_catalog.pg_namespace AS n on n.oid=c.relnamespace where c.relkind in ('v') and n.nspname<>'pg_catalog' and r.rolname='" . $this->db->username . "'")->query();

      if(!$pg_views || !isset($pg_views->rowCount))
        return false;

      $pg_views = $pg_views->readAll();

      return $pg_views;
    }

    public function getView($name){
      $pg_views = $this->db->createCommand("select c.relname AS \"Name\", c.relkind AS \"Type\", r.rolname AS \"Owner\" from pg_catalog.pg_class AS c join pg_catalog.pg_roles AS r on r.oid=c.relowner left join pg_catalog.pg_namespace AS n on n.oid=c.relnamespace where c.relkind in ('v') and n.nspname<>'pg_catalog' and c.relname='" . $name . "' and r.rolname='" . $this->db->username . "'")->query();

      if(!$pg_views || !isset($pg_views->rowCount))
        return false;

      $pg_views = $pg_views->readAll();

      return $pg_views;
    }
}
