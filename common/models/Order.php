<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%order}}".
 *
 * @property integer $id
 * @property integer $id_user
 * @property integer $total_quantity
 * @property string $amount
 * @property string $note
 * @property integer $created_at
 * @property integer $updated_at
 */
class Order extends Model
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return $this->db->quoteTableName('{{%order}}');
    }

    * @inheritdoc
    */
   public function attributeLabels()
   {
       return [
           'id',
           'id_user',
           'total_quantity',
           'amount',
           'note',
           'created_at',
           'updated_at',
       ];
   }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'id_user' => Yii::t('app', 'Id User'),
            'total_quantity' => Yii::t('app', 'Total Quantity'),
            'amount' => Yii::t('app', 'Amount'),
            'note' => Yii::t('app', 'Note'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }
}
