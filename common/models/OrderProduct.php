<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%order_product}}".
 *
 * @property integer $id
 * @property integer $id_order
 * @property integer $id_product
 * @property integer $quantity
 * @property string $price
 * @property integer $created_at
 * @property integer $updated_at
 */
class OrderProduct extends Model
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return $this->db->quoteTableName('{{%order_product}}');
    }

    /**
     * @inheritdoc
     */
    public function attributes()
    {
        return [
            'id',
            'id_order',
            'id_product',
            'quantity',
            'price',
            'created_at',
            'updated_at',
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'id_order' => Yii::t('app', 'Id Order'),
            'id_product' => Yii::t('app', 'Id Product'),
            'quantity' => Yii::t('app', 'Quantity'),
            'price' => Yii::t('app', 'Price'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }
}
