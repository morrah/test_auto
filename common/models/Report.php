<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%report}}".
 *
 * @property integer $id
 * @property string $username
 * @property string $email
 * @property integer $status
 * @property integer $order_id
 * @property integer $total_quantity
 * @property string $amount
 * @property string $note
 * @property integer $id_product
 * @property integer $quantity
 * @property string $price
 * @property string $title
 */
class Report extends Model
{
    public $id;
    public $username;
    public $email;
    public $status;
    public $order_id;
    public $total_quantity;
    public $amount;
    public $note;
    public $id_product;
    public $quantity;
    public $price;
    public $title;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return Yii::$app->db->quoteTableName('{{%report}}');
    }

    /**
     * @inheritdoc
     */
    public function attributes()
    {
        return [
          'id',
          'username',
          'email',
          'status',
          'order_id',
          'total_quantity',
          'amount',
          'note',
          'id_product',
          'quantity',
          'price',
          'title',
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'username' => Yii::t('app', 'Username'),
            'email' => Yii::t('app', 'Email'),
            'status' => Yii::t('app', 'Status'),
            'order_id' => Yii::t('app', 'Order ID'),
            'total_quantity' => Yii::t('app', 'Total Quantity'),
            'amount' => Yii::t('app', 'Amount'),
            'note' => Yii::t('app', 'Note'),
            'id_product' => Yii::t('app', 'Id Product'),
            'quantity' => Yii::t('app', 'Quantity'),
            'price' => Yii::t('app', 'Price'),
            'title' => Yii::t('app', 'Title'),
        ];
    }
}
