<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%product}}".
 *
 * @property integer $id
 * @property string $title
 * @property integer $in_stock
 * @property string $price
 * @property integer $created_at
 * @property integer $updated_at
 */
class Product extends Model
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return $this->db->quoteTableName('{{%product}}');
    }

    /**
     * @inheritdoc
     */
    public function attributes()
    {
        return [
            'id',
            'title',
            'in_stock',
            'price',
            'created_at',
            'updated_at',
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'title' => Yii::t('app', 'Title'),
            'in_stock' => Yii::t('app', 'In Stock'),
            'price' => Yii::t('app', 'Price'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }
}
