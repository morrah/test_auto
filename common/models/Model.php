<?php

namespace common\models;

use Yii;
use \yii\helpers\Inflector;
/**
 * This is the model class for PostgreSQL views.
 *
 * @property integer $id
 * @property string $title
 * @property integer $owner
 */
class Model extends \yii\base\Model
{

    public $db;

    public $table;

    /**
     * Initializes the object.
     * This method is invoked at the end of the constructor after the object is initialized with the
     * given configuration.
     */
    public function init()
    {
      $this->db = Yii::$app->db;

      $this->table = $this->tableName();

    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return $this->db->quoteTableName('{{%' . Inflector::underscore(get_class($this)) . '}}');
    }

    /**
     * @inheritdoc
     */
    public function formName()
    {
        return parent::formName();//Inflector::camelize(get_class($this));
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [];
    }

    /**
     * Fetch rows using pagination, optionally as array
     * @param string | array $fields like "id,username,amount" or ['id','usernsme','amount']
     * @param int $offset
     * @param int $limit
     * @param bool $as_array
     * @return yii\db\DataReader | array
     */
    public function getAll( $fields = [], $offset=0, $limit=10, $as_array = true){
      $limit = $limit < 1 ? 10 : $limit;
      $fields = is_string($fields)?(strlen(trim($fields))?$fields:"*"):(count((array)$fields)?implode(",",(array)$fields):"*");
      $rows = $this->db->createCommand("SELECT * from " . $this->table . " LIMIT " . $limit . " OFFSET " . $offset . "")->query();

      if(!$rows || !isset($rows->rowCount))
        return false;

      if($as_array)
        $rows = $rows->readAll();

      return $rows;
    }

    /**
     * Fetch one row, optionally by condition
     * @param array $condition like ['column_name' => 'value']
     * @return array
     */
    public function getOne($condition = []){

      $condition = (array)$condition;
      foreach($condition as $k => $v){
        $condition_str .= $k . " = '" . strval($v). "' AND ";
      }
      $condition_str = rtrim($condition_str, "AND ");

      $row = $this->db->createCommand("SELECT * FROM " . $this->table . " WHERE " . $condition_str . "")->queryOne();

      return $row;
    }
}
