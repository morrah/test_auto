<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%buyer_grouped}}".
 *
 * @property integer $id
 * @property string $username
 * @property string $email
 * @property integer $status
 * @property integer $count
 * @property integer $total_quantity
 * @property string $amount
 */
class BuyerGrouped extends Model
{
    public $id;
    public $username;
    public $email;
    public $status;
    public $count;
    public $total_quantity;
    public $amount;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return Yii::$app->db->quoteTableName('{{%buyer_grouped}}');
    }

    /**
     * @inheritdoc
     */
    public function attributes()
    {
        return [
            'id',
            'username',
            'email',
            'status',
            'count',
            'total_quantity',
            'amount',
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'username' => Yii::t('app', 'Username'),
            'email' => Yii::t('app', 'Email'),
            'status' => Yii::t('app', 'Status'),
            'count' => Yii::t('app', 'Count'),
            'total_quantity' => Yii::t('app', 'Total Quantity'),
            'amount' => Yii::t('app', 'Amount'),
        ];
    }
}
