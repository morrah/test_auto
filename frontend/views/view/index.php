<?php
Use yii\grid\GridView;
Use yii\helpers\Html;
Use yii\helpers\Url;
/* @var $this yii\web\View */
?>
<h1>Views list</h1>

<div>
    <?php
      echo GridView::widget([
        'dataProvider' => $views,
       'columns' => [
         [
          'attribute' => 'title',
          'format' => 'html',
          'label' => Yii::t('app','View title'),
          'content' => function($model, $key, $index, $column){
            return Html::a(Yii::t('app',$model['title']),Url::to(['view/view/'.$model['title']]));
          }
        ],
     ]]);
    ?>
</div>
