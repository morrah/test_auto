<?php
/* @var $this yii\web\View */
Use yii\grid\GridView;
Use yii\helpers\Html;
Use yii\helpers\Url;
?>
<h1>View <?php echo $view ?></h1>

<?php include("setupForm.php"); ?>

<div class="view">
<?php echo GridView::widget([
    'dataProvider' => $items,
   'columns' => $columns,
 ]) ?>
</div>
