<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
Use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model \common\models\User */
/* @var $form ActiveForm */
?>
<div class="setupForm">

  <h2><?php echo Yii::t('app','Set up this view'); ?></h2>

  <h3><?php echo Yii::t('app','Select fields to show'); ?></h3>

    <?php //var_dump($model);
    //var_dump($fields);
    $form = ActiveForm::begin(['method' => 'GET']);//['action' => Url::to(['view/view/'.$model->title])] ?>

        <?php
        $attrs = $model->attributes();
        foreach($attrs as $k => $v){
          ?>
          <fieldset>
          <?php
          echo $form->field($model, $v,['options' => ['class' => 'col-md-2 pull-left']])->checkbox();
          echo $form->field($model, $v,['options' => ['class' => 'pull-left'], 'labelOptions' => ['class' => 'hidden']])->textInput(['name' => $modelNm.'[labels]['.$v.']', 'value' => !empty($labels[$v])?$labels[$v]:'']);
          ?>
        </fieldset>
          <?php
        }
        ?>

        <div class="form-group">
            <?php echo Html::submitButton(Yii::t('app', 'Submit'), ['class' => 'btn btn-primary']) ?>
        </div>
    <?php ActiveForm::end(); ?>

</div><!-- setupForm -->
