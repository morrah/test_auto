<?php

namespace frontend\controllers;
use Yii;
use yii\web\Controller;
use yii\helpers\Inflector;
Use common\models\View as ViewModel;
Use yii\helpers\Html;
Use yii\helpers\Url;

class ViewController extends Controller
{
    public function actionIndex()
    {
        $model = new ViewModel();
        $views = $model->getAll();
        $provider = new \yii\data\ArrayDataProvider([
            'allModels' => $views,
            'sort' => [
                'attributes' => ['title'],
            ],
            'pagination' => [
                'pageSize' => 5,
            ],
        ]);
        return $this->render('index',['views' => $provider]);
    }

    public function actionView()
    {
        $view = Yii::$app->request->get("view","buyer");
        $modelNm = Inflector::camelize($view);

        $modelName = '\\common\\models\\' . $modelNm;
        $model = new $modelName();
        $attrs = $model->attributes();
        $fields = [];
        $labels = [];
        $formVars = Yii::$app->request->get($modelNm,null);

        foreach($attrs as $v){
          //if(Yii::$app->request->post({$modelNm}[$k],null)!==null) // не могу взять в одну строку, что-то упустила. Поэтому ведро переменных.
          if(isset($formVars[$v]) && $formVars[$v]){

            array_push($fields, $v);

            if(isset($formVars['labels'][$v]) && $formVars['labels'][$v]){
              $labels[$v] = $formVars['labels'][$v];
            }

          }
        }
        unset($formVars['labels']);
        if(is_array($formVars))
          $model->setAttributes($formVars,false);

        $page = Yii::$app->request->get("page",0);
        $limit = Yii::$app->request->get("per-page",10);
        $page=$page?(($page-1)*$limit):0;
        $items = $model->getAll($fields,$page);
        $provider = new \yii\data\ArrayDataProvider([
            "allModels" => $items,
            "sort" => [
                "attributes" => $fields,
            ],
            "pagination" => [
                "pageSize" => 5,
            ],
        ]);
        $columns = [];
        foreach($fields as $field) {
          array_push($columns,
          [
           'attribute' => $field,
           'format' => 'html',
           'label' => isset($labels[$field])?$labels[$field]:(Yii::t('app',$field)),

         ]);
         }

        return $this->render('view',['view' => $view, 'model' => $model, 'items' => $provider, 'columns' => $columns, 'fields' => $fields, 'labels' => $labels, 'modelNm' => $modelNm]);
    }

}
